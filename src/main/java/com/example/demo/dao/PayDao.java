package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface PayDao {
    List<Map> find_all_pay(Map map);
    long count_all_pay(Map map);
    int add_pay(Map map);
    int del_pay(Map map);
    int upd_pay(Map map);
    int search_pay(Map map);
}

