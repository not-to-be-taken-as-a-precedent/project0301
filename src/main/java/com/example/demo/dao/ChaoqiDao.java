package com.example.demo.dao;
import java.util.List;
import java.util.Map;
public interface ChaoqiDao {
    List<Map> find_all_chaoqi(Map map);

    long count_all_chaoqi(Map map);

    int add_chaoqi(Map map);

    int del_chaoqi(Map map);

    int upd_chaoqi(Map map);
}
