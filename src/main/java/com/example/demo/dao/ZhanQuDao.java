package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface ZhanQuDao {
    List<Map> find_all_zhanqu(Map map);
    long count_all_zhanqu(Map map);
    int add_zhanqu(Map map);
    int del_zhanqu(Map map);
    int upd_zhanqu(Map map);
}
