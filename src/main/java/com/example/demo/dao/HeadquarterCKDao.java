package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface HeadquarterCKDao {
    List<Map> find_all_hdwuliao(Map map);
    long count_all_hdwuliao(Map map);
    int add_hdwuliao(Map map);
    int del_hdwuliao(Map map);
    int upd_hdwuliao(Map map);
//    List<Map> getMdByZq(Map map);

}
