package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface WuLiaoDao {
    List<Map> find_all_wuliao(Map map);
    long count_all_wuliao(Map map);
    int add_wuliao(Map map);
    int del_wuliao(Map map);
    int upd_wuliao(Map map);
}
