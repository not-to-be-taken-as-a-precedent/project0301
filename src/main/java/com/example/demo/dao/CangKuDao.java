package com.example.demo.dao;
import java.util.List;
import java.util.Map;

public interface CangKuDao {
    List<Map> find_all_cangku(Map map);
    long count_all_cangku(Map map);
    int add_cangku(Map map);
    int del_cangku(Map map);
    int upd_cangku(Map map);
}