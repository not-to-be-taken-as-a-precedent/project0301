package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface HuiYuanDao {
    List<Map> find_all_huiyuan(Map map);
    long count_all_huiyuan(Map map);
    int add_huiyuan(Map map);
    int del_huiyuan(Map map);
    int upd_huiyuan(Map map);
}
