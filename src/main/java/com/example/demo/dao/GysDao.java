package com.example.demo.dao;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author SJ
 * @Date 2023/2/27 18:27
 */
public interface GysDao {
    List<Map> find_all_gys(Map map);

    long count_all_gys(Map map);

    int add_gys(Map map);

    int del_gys(Map map);

    int upd_gys(Map map);
}
