package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface SystemUserDao {
    List<Map> find_all_systemUser(Map map);
    List<Map> find_systemUser_by_token(String token);
    int add_systemUser(Map map);
    int upd_systemUserToken(Map map);
}
