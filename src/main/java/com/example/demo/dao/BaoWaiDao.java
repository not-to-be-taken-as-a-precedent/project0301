package com.example.demo.dao;
import java.util.List;
import java.util.Map;
public interface BaoWaiDao {
    List<Map> find_all_baowai(Map map);

    long count_all_baowai(Map map);

    int add_baowai(Map map);

    int del_baowai(Map map);

    int upd_baowai(Map map);
}
