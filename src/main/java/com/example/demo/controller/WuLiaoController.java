package com.example.demo.controller;

import com.example.demo.dao.WuLiaoDao;
import com.example.demo.dao.ZhanQuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class WuLiaoController {
    @Autowired
    WuLiaoDao wuLiaoDao;

    @PostMapping("/del_wuliao")
    public int del_wuliao(@RequestBody Map map){
        return wuLiaoDao.del_wuliao(map);
    }

    @PostMapping("/add_wuliao")
    public int add_wuliao(@RequestBody Map map){
        return wuLiaoDao.add_wuliao(map);
    }

    @PostMapping("/find_all_wuliao")
    public List<Map> find_all_wuliao(@RequestBody Map map) throws InterruptedException {

        return wuLiaoDao.find_all_wuliao(map);
    }
}
