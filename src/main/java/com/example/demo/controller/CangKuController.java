package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.CangKuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class CangKuController {
    @Autowired
    CangKuDao cangKuDao;


    @PostMapping("/del_cangku")
    public int del_cangku(@RequestBody Map map){
        return cangKuDao.del_cangku(map);
    }


    @PostMapping("/add_cangku")
    public int add_cangku(@RequestBody Map map){
        return cangKuDao.add_cangku(map);
    }

    @PostMapping("/find_all_cangku")
    public List<Map> find_all_cangku(@RequestBody Map map) throws InterruptedException {
        //thread.sleep(2000);

        return cangKuDao.find_all_cangku(map);
    }
}
