package com.example.demo.controller;

import com.example.demo.dao.SystemUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static javax.swing.UIManager.put;

@RestController
@CrossOrigin
public class SystemUserController {

    @Autowired
    SystemUserDao systemUserDao;

    @PostMapping("/sysuser_reg")
    public boolean register(@RequestBody Map map){
        return systemUserDao.add_systemUser(map)==1;
    }

    @PostMapping("/sysuser_login")
    public String login(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(2000);
        List<Map> users =
                systemUserDao.find_all_systemUser(map);
        if(users.size()==1){
            // token 鐧诲綍鍑嵁
            Map currentUser = users.get(0);
            String token = UUID.randomUUID()+"";
            systemUserDao.upd_systemUserToken(
                    new HashMap(){{
                        put("id",currentUser.get("id"));
                        put("token", token);
                    }}
            );
            return token;
        }else{
            return "no";
        }
    }

}
