package com.example.demo.controller;

import com.example.demo.dao.ChaoqiDao;
import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.ZhanQuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class ChaoqiController {
    @Autowired
    ChaoqiDao chaoqiDao;

    @PostMapping("/del_chaoqi")
    public int del_chaoqi(@RequestBody Map map){
        return chaoqiDao.del_chaoqi(map);
    }

    @PostMapping("/add_chaoqi")
    public int add_chaoqi(@RequestBody Map map){
        return chaoqiDao.add_chaoqi(map);
    }

    @PostMapping("/find_all_chaoqi")
    public List<Map> find_all_chaoqi(@RequestBody Map map) throws InterruptedException {

        return chaoqiDao.find_all_chaoqi(map);
    }
}
