package com.example.demo.controller;

import com.example.demo.dao.BaoWaiDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class BaoWaiController {
    @Autowired
    BaoWaiDao baowaiDao;

    @PostMapping("/del_baowai")
    public int del_baowai(@RequestBody Map map){
        return baowaiDao.del_baowai(map);
    }

    @PostMapping("/add_baowai")
    public int add_baowai(@RequestBody Map map){
        return baowaiDao.add_baowai(map);
    }

    @PostMapping("/find_all_baowai")
    public List<Map> find_all_baowai(@RequestBody Map map) throws InterruptedException {

        return baowaiDao.find_all_baowai(map);
    }
}