package com.example.demo.controller;

import com.example.demo.dao.PayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class PayController {
    @Autowired
    PayDao payDao;

    @PostMapping("/add_pay")
    public int add_pay(@RequestBody Map map){
        return payDao.add_pay(map);
    }

    @PostMapping("/del_pay")
    public int del_pay(@RequestBody Map map){
        return payDao.del_pay(map);
    }

    @PostMapping("/upd_pay")
    public int upd_pay(@RequestBody Map map){
        return payDao.upd_pay(map);
    }

    @PostMapping("/search_pay")
    public int search_pay(@RequestBody Map map){
        return payDao.search_pay(map);
    }

    @PostMapping("/find_all_pay")
    public List<Map> find_all_pay(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(2000);
        return payDao.find_all_pay(map);
    }
}
