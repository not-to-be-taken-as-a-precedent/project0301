package com.example.demo.controller;

import com.example.demo.dao.HeadquarterCKDao;
import com.example.demo.dao.MenDianDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class HeadquarterController {
    @Autowired
    HeadquarterCKDao headquarterCKDao;

    @PostMapping("/find_all_hdwuliao")
    public List<Map> find_all_hdwuliao(@RequestBody Map map) throws InterruptedException{
        return headquarterCKDao.find_all_hdwuliao(map);
    }
    @PostMapping("/add_hdwuliao")
    public int add_hdwuliao(@RequestBody Map map){
        return headquarterCKDao.add_hdwuliao(map);
    }
    @PostMapping("/del_hdwuliao")
    public int del_hdwuliao(@RequestBody Map map){
        return headquarterCKDao.del_hdwuliao(map);
    }
    @PostMapping("/upd_hdwuliao")
    public int upd_hdwuliao(@RequestBody Map map){
        return headquarterCKDao.upd_hdwuliao(map);
    }

//    @PostMapping("/getMdByZq")
//    public List<Map> getMdByZq(@RequestBody Map map) throws InterruptedException{
//        return menDianDao.getMdByZq(map);
//    }

}
