package com.example.demo.controller;

import com.example.demo.dao.ZhanQuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class ZhanQuController {
    @Autowired
    ZhanQuDao zhanQuDao;

    @PostMapping("/del_zhanqu")
    public int del_zhanqu(@RequestBody Map map){
        return zhanQuDao.del_zhanqu(map);
    }

    @PostMapping("/add_zhanqu")
    public int add_zhanqu(@RequestBody Map map){
        return zhanQuDao.add_zhanqu(map);
    }

    @PostMapping("/find_all_zhanqu")
    public List<Map> find_all_zhanqu(@RequestBody Map map) throws InterruptedException {

        return zhanQuDao.find_all_zhanqu(map);
    }
}
