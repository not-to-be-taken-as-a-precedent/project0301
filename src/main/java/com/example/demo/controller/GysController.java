package com.example.demo.controller;

import com.example.demo.dao.GysDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author SJ
 * @Date 2023/2/27 18:32
 */
@RestController
public class GysController {
    @Autowired
    GysDao gysDao;

    @PostMapping("/del_gys")
    public int del_zhanqu(@RequestBody Map map){
        return gysDao.del_gys(map);
    }

    @PostMapping("/add_gys")
    public int add_gys(@RequestBody Map map) {
        return gysDao.add_gys(map);
    }

    @PostMapping("/find_all_gys")
    public List<Map> find_all_gys(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(2000);
        return gysDao.find_all_gys(map);
    }
}
