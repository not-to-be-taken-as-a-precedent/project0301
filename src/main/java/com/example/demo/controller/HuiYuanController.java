package com.example.demo.controller;

import com.example.demo.dao.HuiYuanDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class HuiYuanController {
    @Autowired
    HuiYuanDao huiYuanDao;

    @PostMapping("/del_huiyuan")
    public int del_huiyuan(@RequestBody Map map){
        return huiYuanDao.del_huiyuan(map);
    }

    @PostMapping("/add_huiyuan")
    public int add_huiyuan(@RequestBody Map map){
        return huiYuanDao.add_huiyuan(map);
    }

    @PostMapping("/find_all_huiyuan")
    public List<Map> find_all_huiyuan(@RequestBody Map map) throws InterruptedException {

        return huiYuanDao.find_all_huiyuan(map);}
}
