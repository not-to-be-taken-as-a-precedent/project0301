﻿var datas = [];

loadData();

loadBaoWaiDan();

function loadBaoWaiDan(){
    ajax("/find_all_baowaidan",{},function(data){
        
        for(var i=0;i<data.length;i++){
           var d = data[i];
            baowaidan_input.innerHTML += "<option value='"+d.id+"'>"+d.name+"</option>";
        }
    });
}

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>订单类型</th><th>子订单名称</th><th>厂家单号</th>" +
        "<th>创建时间</th><th>修改时间</th><th>状态</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.leixin+"</td>" +
            "<td class='ntitle'>"+a.zileixin+"</td>" +
            "<td class='ntitle'>"+a.danhao+"</td>" +
            "<td class='ntitle'>"+a.chuangjian_time+"</td>" + "<td class='ntitle'>"+a.xiugai_time+"</td>" + "<td class='ntitle'>"+a.zhuangtai+"</td>" +
            "<td class='ntitle'><a class='edit_a' href='#'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;


}

function search(){
    loadData({name:stitle.value,code:stitle.value});
}

function loadData(data){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_baowaidan",data,function(data){
         datas = data;
         showData(datas);
         loading.style.display = "none";
    });
}

function save(){
    ajax("/add_baowaidan",{
        leixin:leixin.value,
        zileixin:zileixin.value,
        danhao:danhao.value,
        chuangjian_time:chuangjian_time.value,
        xiugai_time:xiugai_time.value,
        zhuangtai:zhuangtai.value
    },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}


function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_baowaidan", {id:id},function(data){
       alert("删除成功！");
       loadData();
    });
}

function add(){
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
}