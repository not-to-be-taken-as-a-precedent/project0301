var gyss = [];
loadData();

function showData(arr) {
    list_div.innerHTML = "";
    if (arr.length == 0) {
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>供应商名称</th><th>供应商编码</th><th>供应商负责人</th>" +
        "<th>负责人联系方式</th><th>供应商类型</th><th>供应商状态</th><th>操作</th></tr>";
    for (var i = arr.length - 1; i >= 0; i--) {
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>" + a.gys_name + "</td>" +
            "<td class='ntitle'>" + a.gys_code + "</td>" +
            "<td class='ntitle'>" + a.admin_name + "</td>" +
            "<td class='ntitle'>" + a.admin_tel + "</td>" +
            "<td class='ntitle'>" + a.gys_type + "</td>" +
            "<td class='ntitle'>" + a.gys_status + "</td>" +
            "<td class='ntitle'><a class='edit_a' onclick='bianji()' href='javascript:;'>编辑</a>" +
            "<a class='del_a' onclick='del(" + a.id + ")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

function search() {
    loadData({gys_name: stitle.value, gys_code: stitle.value});
}

function bianji() {
    alert("抱歉，您没有权限");
}

function loadData(data) {
    list_div.innerHTML = "";
    loading.style.display = "block";
    var req = new XMLHttpRequest();
    req.open("post", "/find_all_gys");
    req.setRequestHeader("content-Type", "application/json;charset=utf-8");
    if (!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200) {
            gyss = JSON.parse(req.responseText);
            showData(gyss);
            loading.style.display = "none";
        }
    }
}

function save() {
    ajax("/add_gys", {
        gys_name: gys_name.value,
        gys_code: gys_code.value,
        admin_name: admin_name.value,
        admin_tel: admin_tel.value,
        gys_type: gys_type.value,
        gys_status: gys_status.value
    }, function (data) {
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}


function del(id) {
    if (!confirm("您确定要删除这个数据吗？")) {
        return;
    }
    var req = new XMLHttpRequest();
    req.open("post", "/del_gys");
    req.setRequestHeader("content-Type", "application/json;charset=utf-8");
    var data = {
        id: id
    }
    req.send(JSON.stringify(data));
    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200) {
            if (req.responseText.trim() == "1") {
                alert("删除成功！");
            }
            loadData();
        }
    }
}

function add() {
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
}