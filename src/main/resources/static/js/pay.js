var pays = [];
loadData();

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }

    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>支付名称</th><th>支付编码</th><th>渠道名称</th>" +
        "<th>状态</th><th>创建时间</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.payname+"</td>" +
            "<td class='ntitle'>"+a.paycode+"</td>" +
            "<td class='ntitle'>"+a.qudao_name+"</td>" +
            "<td class='ntitle'>"+a.paystatus+"</td>" +
            "<td class='ntitle'>"+a.paytime+"</td>" +
            "<td class='ntitle'><a class='edit_a' onclick='upd("+a.id+")' href='#'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML =allhtml;


}

function search(){
    loadData({payname:stitle.value,paycode:stitle.value});
}


function save(){
    ajax("/add_pay",{
        payname:payname.value,
        paycode:paycode.value,
        qudao_name:qudao_name.value,
        paystatus:paystatus.value,
        paytime:paytime.value,
    },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}

function upd(id){
    if(!confirm("您确定要编辑这个数据吗？")){
        return;
    }
    ajax("/upd_pay",{
            payname:payname.value,
            paycode:paycode.value,
            qudao_name:qudao_name.value,
            paystatus:paystatus.value,
            paytime:paytime.value,
        },function(data){
            alert("修改成功！");
            loadData();
            add_div.style.display = "none";
            list_div.style.display = "block";
            save_btn.style.display = "none";
            add_btn.style.display = "block";
        })
}

function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    var req = new XMLHttpRequest();
    req.open("post","/del_pay");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    var data = {
        id:id
    }
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            if(req.responseText.trim()=="1"){
                alert("删除成功！");
            }

            loadData();
        }
    }
}

function add(){
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
    search_div.style.display = "none";
    return_btn.style.display = "block";
}

function returnMain(){
    add_div.style.display = "none";
    list_div.style.display = "block";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
    search_div.style.display = "block";
    return_btn.style.display = "none";
}

function loadData(data){
    list_div.innerHTML = "";
    loading.style.display = "block";
    var req = new XMLHttpRequest();
    req.open("post","/find_all_pay");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            pays = JSON.parse(req.responseText);
            showData(pays);
            loading.style.display = "none";
        }
    }
}