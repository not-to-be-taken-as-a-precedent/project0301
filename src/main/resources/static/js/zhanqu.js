var zhanqus = [];
loadData();

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>战区名称</th><th>战区编码</th><th>负责人</th>" +
        "<th>负责人联系方式</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.name+"</td>" +
            "<td class='ntitle'>"+a.code+"</td>" +
            "<td class='ntitle'>"+a.admin_name+"</td>" +
            "<td class='ntitle'>"+a.admin_tel+"</td>" +
            "<td class='ntitle'><a class='edit_a' href='#'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

function search(){
    loadData({name:stitle.value,code:stitle.value});
}

function loadData(data){
    list_div.innerHTML = "";
    loading.style.display = "block";
    var req = new XMLHttpRequest();
    req.open("post","/find_all_zhanqu");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            zhanqus = JSON.parse(req.responseText);
            showData(zhanqus);
            loading.style.display = "none";
        }
    }
}

function save(){
    ajax("/add_zhanqu",{
        name:zname.value,
        code:code.value,
        admin_name:admin_name.value,
        admin_tel:admin_tel.value
    },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}


function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    var req = new XMLHttpRequest();
    req.open("post","/del_zhanqu");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    var data = {
        id:id
    }
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            if(req.responseText.trim()=="1"){
                alert("删除成功！");
            }

            loadData();
        }
    }
}

function add(){
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
}