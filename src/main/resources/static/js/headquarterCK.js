var hdwuliaos = [];
var wuliao={};
loadData();

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'>" +
        "<tr class='theader'>" +
        "<th>物料名称</th>" +
        "<th>物料编码</th>" +
        "<th>物料SN号</th>" +
        "<th>物料类型</th>" +
        "<th>物料仓库</th>"+
        "<th>供应商类型</th>"+
        "<th>供应商名称</th>"+
        "<th>供应商编码</th>"+
        "<th>成本价</th>"+
        "<th>状态</th>"+
        "<th>操作</th>" +
        "</tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.name+"</td>" +
            "<td class='ntitle'>"+a.code+"</td>" +
            "<td class='ntitle'>"+a.sn_no+"</td>" +
            "<td class='ntitle'>"+a.type+"</td>" +
            "<td class='ntitle'>"+a.wlcangku+"</td>" +
            "<td class='ntitle'>"+a.sup_type+"</td>" +
            "<td class='ntitle'>"+a.sup_name+"</td>" +
            "<td class='ntitle'>"+a.sup_code+"</td>" +
            "<td class='ntitle'>"+a.chengben+"</td>" +
            "<td class='ntitle'>"+a.status+"</td>" +
            "<td class='ntitle'>" +
            "<a class='edit_a' onclick='edit("+i+")' href='javascript:;'>上架/下架</a>" +
            "<a class='edit_a' onclick='edit("+i+")' href='javascript:;'>调架</a>" +
            "<a class='edit_a' onclick='edit("+i+")' href='javascript:;'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a>" +
            "</td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}
function searchByZq(zq_id){
    console.log(zq_id);
    if(zq_id=="all") loadData();
    else
        ajax("/getMdByZq",{
            zq_id:zq_id
        },function (mendians){
            showData(mendians);
        })
}
function search(){
    loadData({name:stitle.value,code:stitle.value});
}
function loadZhanqu(data){
    var req = new XMLHttpRequest();
    req.open("post","/find_all_zhanqu");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            zhanqus = JSON.parse(req.responseText);
            var s="<option value='all'>全部</option>";
            for(var i=0;i<zhanqus.length;i++){
                zhanqu=zhanqus[i];
                s+="<option value='"+zhanqu.id+"'>"+zhanqu.name+"</option>";
                console.log(zhanqu);
            }
            s_zq.innerHTML=s;
        }
    }
}

function loadData(data){
    list_div.innerHTML = "";
    loading.style.display = "block";
    var req = new XMLHttpRequest();
    req.open("post","/find_all_hdwuliao");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            hdwuliaos = JSON.parse(req.responseText);
            showData(hdwuliaos);
            loading.style.display = "none";
        }
    }
}

function load_WLtype(data){
    var req = new XMLHttpRequest();
    req.open("post","/find_all_wuliao");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            var wuliao_type = JSON.parse(req.responseText);
            var type;
            var s="";
            for(var i=0;i<wuliao_type.length;i++){
                type=wuliao_type[i].type;
                s+="<option >"+type+"</option>";
            }
            wl_type.innerHTML=s;
        }
    }
}

function load_WLCK(data){
    var req = new XMLHttpRequest();
    req.open("post","/find_all_cangku");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            var CKS = JSON.parse(req.responseText);
            var ck;
            var s="";
            for(var i=0;i<CKS.length;i++){
                ck=CKS[i].name;
                s+="<option >"+ck+"</option>";
            }
            wl_CK.innerHTML=s;
        }
    }
}



function add(){
    loading.style.display="none";
    add_div.style.display = "block";
    list_div.style.display = "none";
    add_save_btn.style.display = "block";
    add_btn.style.display = "none";
    load_WLtype();
    load_WLCK();
}
function add_save(){
    ajax("/add_hdwuliao",{
        name:wl_name.value,
        code:code.value,
        type:wl_type.value,
        sn_no:sn_no.value,
        sup_code:sup_code.value,
        wlcangku:wl_CK.value,
        chengben:chengben.value,
        status:status_sel.value
    },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        add_save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}
function edit(md_index) {
    var mendian = mendians[md_index];
    edit_div.style.display = "block";
    list_div.style.display = "none";
    edit_save_btn.style.display = "block";
    add_btn.style.display = "none";
    e_md_name.value = mendian.m_name;
    e_md_code.value = mendian.m_code;
    e_admin_name.value = mendian.admin_name;
    e_admin_tel.value = mendian.admin_tel;
    fault_sel.value=mendian.status;
    var op_html = "所属战区: <select id='e_zq_select'>";
    for (var i = 0; i < zhanqus.length; i++) {
        var name = zhanqus[i].name;
        op_html +=
            "<option>" + name + "</option>";
    }
    op_html+="</select>";
    e_option_zq.innerHTML = op_html;
    edit_md_id = mendian.m_id;
}
function edit_save(){
    for(var i=0;i<zhanqus.length;i++){
        zhanqu=zhanqus[i];
        if(zhanqu.name==e_zq_select.value){
            edit_zq_id=zhanqu.id;
            break;
        }
    }
    console.log(e_zq_select.value);
    console.log(e_status.value);
    console.log(edit_zq_id);
    console.log(edit_md_id);
    ajax("/upd_mendian",{
        id:edit_md_id,
        name:e_md_name.value,
        code:e_md_code.value,
        admin_name:e_admin_name.value,
        admin_tel:e_admin_tel.value,
        zhanqu:edit_zq_id,
        status:e_status.value
    },function (data){
        alert("编辑成功！");
        loadData();
        edit_div.style.display = "none";
        list_div.style.display = "block";
        edit_save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}
function del(id){
    if(!confirm("您确定要删除这个门店吗？")){
        return;
    }
    var req = new XMLHttpRequest();
    req.open("post","/del_mendian");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    var data = {
        id:id
    }
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            if(req.responseText.trim()=="1"){
                alert("删除成功！");
            }
            loadData();
        }
    }
}


